from math import floor
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button


class View(FloatLayout):
    buttons_names = [
        "C", "+/-", "%", "/",
        7, 8, 9, "*",
        4, 5, 6, "-",
        1, 2, 3, "+",
        0, ".", "="
    ]

    def __init__(self, controller, **kwargs):
        super().__init__(**kwargs)
        self.controller = controller
        self._buttons = self._create_buttons()
        self._textInput = self._createTextInput()
        self._current_text = ""

    @property
    def buttons(self):
        return self._buttons

    @property
    def current_text(self):
        return self._current_text

    @property
    def textInput(self):
        return self._textInput

    @buttons.setter
    def buttons(self, b: list):
        self._buttons = b

    @current_text.setter
    def current_text(self, txt):
        self._current_text = txt
        self._textInput.text = txt

    def _create_buttons(self):
        bList = []
        for i in range(len(self.buttons_names)):
            bgcolor = (.3, .3, .3, 1)
            bg2color = (.6, .6, .6, 1)
            color = (1, 1, 1, 1)
            if i % 4 / 4 == 0.75 or i == 18:
                bgcolor = (1, .7, 0, 1)
                bg2color = (1, 1, 0, 1)
                color = (0, 0, 0, 1)
            elif i < 4 or i == 17:
                bgcolor = (.8, .8, .8, .8)
                bg2color = (1, 1, 1, 1)
                color = (0, 0, 0, 1)
            is_int = False
            if isinstance(self.buttons_names[i], int):
                is_int = True
            if i > 15:
                if self.buttons_names[i] == 0:
                    b = self._create_button(.5, .15, i * .25 % 1, .6 - .15 * floor(i / 4), color, bgcolor, bg2color,
                                            str(self.buttons_names[i]), is_int)
                else:
                    b = self._create_button(.25, .15, .25 + (i * .25 % 1), .6 - .15 * floor(i / 4), color, bgcolor,
                                            bg2color, str(self.buttons_names[i]), is_int)
            else:
                b = self._create_button(.25, .15, i * .25 % 1, .6 - .15 * floor(i / 4), color, bgcolor, bg2color,
                                        str(self.buttons_names[i]), is_int)
            bList.append(b)
        return bList.copy()

    def _create_button(self, width, height, x, y, color, bg, bg2, content="", is_int=False):
        button = CustomButton(size_hint=(width, height), pos_hint={"x": x, "y": y}, text=content, color=color,
                              background_color=bg, valign="center", halign="center")
        button.text_is_int = is_int
        button.original_bg_color = bg
        button.pressed_bg_color = bg2
        self.add_widget(button)
        button.bind(on_press=self.controller.on_button_press)
        return button

    def _createTextInput(self):
        textInput = TextInput(size_hint=(1, .25), pos_hint={"x": 0, "y": .75}, halign="right", readonly=True,
                              use_handles=False)
        self.add_widget(textInput)
        return textInput

    def change_font_size(self, window=None, width=None, height=None, first=False):
        for button in self._buttons:
            if first:
                if button.text == "0":
                    button.width = self.window.size[0] / 2
                else:
                    button.width = self.window.size[0] / 4
                button.height = self.window.size[1] * .75 / 5
            button.text_size = (button.width, button.height)
            button.font_size = button.text_size[1] * .7  # (button.height + button.width) * .2
            if button.font_size > button.text_size[0] * .7:
                button.font_size = button.text_size[0] * .7
        self.textInput.font_size = self.textInput.size[1] * .4


class CustomButton(Button):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.background_normal = 'atlas://images/defaulttheme/button'
        self._text_is_int = False

    def on_press(self):
        self.background_color = self.pressed_bg_color
        self.background_down = self.background_normal

    def on_touch_up(self, touch):
        self.background_color = self.original_bg_color
        super().on_touch_up(touch)

    @property
    def text_is_int(self):
        return self._text_is_int

    @text_is_int.setter
    def text_is_int(self, value: bool):
        self._text_is_int = value
