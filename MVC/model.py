
class Model():

    def __init__(self, view):
        self.view = view
        self._value = ""
        self._previous_value = ""
        self._operator = ""
        self._previous_operator = ""
        self._is_result = False

    @property
    def value(self):
        return self._value

    @property
    def previous_value(self):
        return self._previous_value

    @property
    def operator(self):
        return self._operator

    @property
    def previous_operator(self):
        return self._previous_operator

    @property
    def is_result(self):
        return self._is_result

    @value.setter
    def value(self, value):
        self._value = value

    @previous_value.setter
    def previous_value(self, value):
        self._previous_value = value

    @operator.setter
    def operator(self, value):
        self._operator = value

    @previous_operator.setter
    def previous_operator(self, value):
        self._previous_operator = value

    @is_result.setter
    def is_result(self, value: bool):
        self._is_result = value

    def calculate(self, button):
        txt = button.text
        if self.value == "ERROR DIVISION BY ZERO":
            self.value = ""
        if button.text_is_int:
            if not self._is_result:
                self.value += str(txt)
        elif txt == "C":
            self.value = ""
            self.previous_value = ""
            self.operator = ""
            self.is_result = False
        elif txt == "+/-":
            if self.value:
                self.value = self.value[1:] if self.value[0] == "-" else "-" + self.value
            else:
                self.value = "-"
        elif txt == ".":
            if "." not in self.value:
                self.value += "."
        else:
            if self.value and self.value != "-":
                if txt == "%":
                    if "e" in self.value:
                        value = float(format(float(self.value), 'f'))
                    else:
                        value = float(self.value) if "." in self.value else int(self.value)
                    self.value = str(value / 100)
                elif txt == "=":
                    if self.previous_value and self.value:
                        self._get_result()
                else:
                    self.previous_operator = self.operator
                    self.operator = txt
                    self.is_result = False
                    if self.previous_value:
                        self._get_result(keep_pv=True)
                    else:
                        self.previous_value = self.value
                        self.value = ""
            elif self.previous_value and self.operator and txt is not "%" and txt is not "=":
                self.operator = txt
        self.view.current_text = str(self.previous_value) + str(self.operator) + str(self.value)

    def _get_result(self, keep_pv=False):
        if "e" in self.previous_value:
            self.previous_value = format(float(self.value), 'f')
        if "e" in self.value:
            self.value = format(float(self.value), 'f')
        pv = float(self.previous_value) if "." in self.previous_value else int(self.previous_value)
        v = float(self.value) if "." in self.value else int(self.value)
        operator = self.previous_operator if self.previous_operator else self.operator
        if operator == "/":
            try:
                result = pv / v
            except ZeroDivisionError:
                self.value = "ERROR DIVISION BY ZERO"
                self.previous_value = ""
                self.operator = ""
                self.previous_operator = ""
                return
        elif operator == "*":
            result = pv * v
        elif operator == "-":
            result = pv - v
        else:
            result = pv + v
        if float(result) == int(result):
            result = int(result)
        if keep_pv:
            self.previous_value = str(result)
            self.value = ""
        else:
            self.previous_value = ""
            self.operator = ""
            self.value = str(result)
        if not self.previous_operator:
            self.is_result = True
        self.previous_operator = ""
