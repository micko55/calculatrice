from MVC.view import View
from MVC.model import Model
from kivy.core.window import Window


class Controller():

    def __init__(self):
        self.view = View(controller=self)
        self.model = Model(self.view)
        Window.minimum_width = 200
        Window.minimum_height = 200
        Window.size = (400, 400)
        self.view.window = Window
        Window.bind(on_show=self.view.change_font_size,
                    on_resize=self.view.change_font_size,
                    on_maximize=self.view.change_font_size,
                    on_restore=self.view.change_font_size)
        self.view.change_font_size(first=True)

    def screen(self):
        return self.view

    def on_button_press(self, instance):
        self.model.calculate(instance)
