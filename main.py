from kivy.app import App
from MVC.controller import Controller


class Calculator(App):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.controller = Controller()

    def build(self):
        self.title = "Calculatrice"
        return self.controller.screen()


if __name__ == "__main__":
    Calculator().run()
